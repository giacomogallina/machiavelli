//use std::io;
//use std::io::Write;
use std::sync::mpsc::{ Receiver, channel };
use std::thread;

pub mod card_set;
use card_set::CardSet;

mod tris_set;
use tris_set::TrisSet;

mod cache;
use cache::Cache;


const TRIS_N: usize = 197;

const fn tris_init() -> [CardSet; TRIS_N] {
    let mut v = [CardSet::new(); TRIS_N];
    let mut l = 3;
    let mut idx = 0;
    while l <= 5 {
        let mut b = 0;
        while b + l - 1 <= 13 {
            let mut s = 0;
            while s < 4 {
                let mut i = 0;
                while i < l {
                    v[idx] = v[idx].merge(CardSet::from_card((b + i) % 13 + 13 * s));
                    i += 1;
                }
                s += 1;
                idx += 1;
            }
            b += 1;
            if l == 13 {
                break;
            }
        }
        l += 1;
    }
    let mut n = 0;
    while n < 13 {
        let mut excl = 0;
        while excl < 5 {
            let mut s = 0;
            while s < 4 {
                if s != excl {
                    v[idx] = v[idx].merge(CardSet::from_card(n + 13 * s));
                }
                s += 1;
            }
            excl += 1;
            idx += 1;
        }
        n += 1;
    }
    v
}

const TRIS: [CardSet; TRIS_N] = tris_init();

const fn tris_with_card_init() -> [TrisSet; 52] {
    let mut arr: [TrisSet; 52] = [TrisSet::new(); 52];
    let mut c = 0;
    while c < 52 {
        let mut i = 0;
        while i < TRIS_N {
            if TRIS[i].contains_card(c) {
                arr[c] = arr[c].set(i);
            }
            i += 1;
        }
        c += 1;
    }
    arr
}


const TRIS_WITH_CARD: [TrisSet; 52] = tris_with_card_init();


//fn explore_tree(remaining_cards: CardSet, hand: CardSet, tris_idx: usize, choosen_trises: &mut Vec<CardSet>, best_trises: &mut Vec<CardSet>, best_score: &mut u32) {
    //if tris_idx < TRIS_N {
        //if remaining_cards.contains(TRIS[tris_idx]) {
            //choosen_trises.push(TRIS[tris_idx]);
            //explore_tree(remaining_cards.remove(TRIS[tris_idx]), hand, tris_idx, choosen_trises, best_trises, best_score);
            //choosen_trises.pop();
        //}
        //explore_tree(remaining_cards, hand, tris_idx + 1, choosen_trises, best_trises, best_score);
    //} else {
        //if hand.contains(remaining_cards) {
            //if remaining_cards.score() < *best_score {
                //*best_score = remaining_cards.score();
                //*best_trises = choosen_trises.clone();
            //}
        //}
    //}
//}

fn hardest_card(remaining_cards: CardSet, hand: CardSet, possible_tris_with_card: &[TrisSet; 52]) -> Option<(usize, bool, usize)> {
    let table = remaining_cards.remove(hand);
    remaining_cards
        .map(|c| (c, table.contains_card(c), possible_tris_with_card[c].count_tris()) )
        .min_by_key(|&(_, on_table, n)| 
                n + if on_table { 0 } else {
                    TRIS_N + (n == 0) as usize * TRIS_N
                    //1<<8 | ((n == 0) as usize) << 9
                }
        )
}

fn update_possible_tris(t: CardSet, remaining_cards: CardSet, possible_tris_with_card: &mut [TrisSet; 52]) -> (CardSet, TrisSet) {
    let mut lost_cards = t;
    lost_cards.cards &= !remaining_cards.doubles;
    let lost_tris = lost_cards.fold(TrisSet::new(), |acc, lc| acc.merge(possible_tris_with_card[lc]));
    let affected_cards = lost_tris.fold(CardSet::new(), |acc, i| acc.merge(TRIS[i]));
    for ac in affected_cards {
        possible_tris_with_card[ac] = possible_tris_with_card[ac].remove(lost_tris);
    }
    (affected_cards, lost_tris)
}

fn restore_possible_tris(affected_cards: CardSet, lost_tris: TrisSet, possible_tris_with_card: &mut [TrisSet; 52]) {
    for ac in affected_cards {
        possible_tris_with_card[ac] = possible_tris_with_card[ac].merge(lost_tris.intersect(TRIS_WITH_CARD[ac]));
    }
}

fn place(remaining_cards: CardSet, hand: CardSet, cards_to_keep: CardSet, possible_tris_with_card: &mut [TrisSet; 52], choosen_trises: &mut Vec<CardSet>, best_trises: &mut Vec<CardSet>, best_score: &mut u32, cache: &mut Cache) {
    if cache.update(remaining_cards, cards_to_keep) { return; }
    match hardest_card(remaining_cards, hand, possible_tris_with_card) {
        Some((_, true, 0)) => (), //println!("can't place table"),
        None | Some((_, false, 0)) => {
            let current_score = remaining_cards.merge(cards_to_keep).score();
            //println!("found combination with score {}", current_score);
            if current_score < *best_score {
                *best_score = current_score;
                *best_trises = choosen_trises.clone();
            }
        },
        Some((c, on_table, _n)) => {
            for t in possible_tris_with_card[c] {
                let t = TRIS[t];
                let (affected_cards, lost_tris) = update_possible_tris(t, remaining_cards, possible_tris_with_card);
                choosen_trises.push(t);
                place(
                    remaining_cards.remove(t),
                    hand,
                    cards_to_keep,
                    possible_tris_with_card,
                    choosen_trises, best_trises, best_score, cache
                );
                choosen_trises.pop();
                restore_possible_tris(affected_cards, lost_tris, possible_tris_with_card);
            }
            if !on_table {
                let c = CardSet::from_card(c);
                let (affected_cards, lost_tris) = update_possible_tris(c, remaining_cards, possible_tris_with_card);
                place(
                    remaining_cards.remove(c),
                    hand,
                    cards_to_keep.merge(c),
                    possible_tris_with_card,
                    choosen_trises, best_trises, best_score, cache
                );
                restore_possible_tris(affected_cards, lost_tris, possible_tris_with_card);
            }
        },
    }
}

#[derive(Clone, Debug)]
struct PlacingJob {
    remaining_cards: CardSet,
    hand: CardSet,
    cards_to_keep: CardSet,
    possible_tris_with_card: [TrisSet; 52],
    choosen_trises: Vec<CardSet>,
}

impl PlacingJob {
    fn new(table: CardSet, hand: CardSet) -> Self {
        let all_cards = table.merge(hand);
        let mut possible_tris_with_card: [TrisSet; 52] = [TrisSet::new(); 52];
        for c in 0..52 {
            for (i, t) in TRIS.iter().enumerate() {
                if t.contains_card(c) && all_cards.contains(*t) {
                    possible_tris_with_card[c] = possible_tris_with_card[c].set(i);
                }
            }
        }
        PlacingJob {
            remaining_cards: all_cards,
            hand,
            cards_to_keep: CardSet::new(),
            possible_tris_with_card,
            choosen_trises: vec![],
        }
    }

    fn split(mut self) -> Vec<Self> {
        match hardest_card(self.remaining_cards, self.hand, &self.possible_tris_with_card) {
            Some((_, _, 0)) | None => vec![self],
            Some((c, on_table, _n)) => {
                let mut res = vec![];
                for t in self.possible_tris_with_card[c] {
                    let t = TRIS[t];
                    let (affected_cards, lost_tris) = update_possible_tris(t, self.remaining_cards, &mut self.possible_tris_with_card);
                    self.choosen_trises.push(t);
                    res.push(PlacingJob {
                        remaining_cards: self.remaining_cards.remove(t),
                        hand: self.hand,
                        cards_to_keep: self.cards_to_keep,
                        choosen_trises: self.choosen_trises.clone(),
                        possible_tris_with_card: self.possible_tris_with_card.clone(),
                    });
                    self.choosen_trises.pop();
                    restore_possible_tris(affected_cards, lost_tris, &mut self.possible_tris_with_card);
                }
                if !on_table {
                    let c = CardSet::from_card(c);
                    update_possible_tris(c, self.remaining_cards, &mut self.possible_tris_with_card);
                    res.push(PlacingJob {
                        remaining_cards: self.remaining_cards.remove(c),
                        hand: self.hand,
                        cards_to_keep: self.cards_to_keep.merge(c),
                        choosen_trises: self.choosen_trises.clone(),
                        possible_tris_with_card: self.possible_tris_with_card.clone(),
                    });
                    res.push(self.clone());
                }
                res
            },
        }
    }

    fn best_move(&mut self, cache: &mut Cache) -> Option<(u32, Vec<CardSet>)> {
        let mut best_trises = vec![];
        let mut best_score = self.hand.score();

        place(
            self.remaining_cards,
            self.hand,
            self.cards_to_keep,
            &mut self.possible_tris_with_card,
            &mut self.choosen_trises,
            &mut best_trises,
            &mut best_score,
            cache,
        );

        if best_score < self.hand.score() {
            Some((best_score, best_trises))
        } else {
            None
        }
    }
}

pub enum BestMoveFeedback {
    Progress(f32),
    NewBestScore(u32, Vec<CardSet>),
    Done,
}

pub fn best_move(table: CardSet, hand: CardSet) -> Receiver<BestMoveFeedback> {
    let (tx, rx) = channel();
    thread::spawn(move || {
        let mut placing_jobs = vec![PlacingJob::new(table, hand)];
        let mut old_jobs_len = 0;
        while placing_jobs.len() > old_jobs_len && placing_jobs.len() < 10000 {
            old_jobs_len = placing_jobs.len();
            placing_jobs = placing_jobs.into_iter().flat_map(|pj| pj.split()).collect();
        }
        let mut best_score = hand.score();
        let n = placing_jobs.len() as f32;
        let mut cache = Cache::new();
        for (i, pj) in placing_jobs.iter_mut().enumerate() {
            if let Some((score, best_trises)) = pj.best_move(&mut cache) {
                if score < best_score {
                    best_score = score;
                    if let Err(_) = tx.send(BestMoveFeedback::NewBestScore(score, best_trises)) {
                        break;
                    }
                }
            }
            if let Err(_) = tx.send(BestMoveFeedback::Progress((i+1) as f32 / n)) {
                break;
            }
        }
        let _ = tx.send(BestMoveFeedback::Done);
    });
    rx
}
