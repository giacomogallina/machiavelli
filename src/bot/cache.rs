use crate::bot::card_set::CardSet;

//const CACHE_SIZE: u64 = 100_000_007;
const CACHE_SIZE: u64 = 10_000_019;
const EMPTY: (CardSet, CardSet) = (CardSet { cards: u64::MAX, doubles: u64::MAX }, CardSet { cards: u64::MAX, doubles: u64::MAX });

pub struct Cache {
    arr: Vec<(CardSet, CardSet)>,
}

impl Cache {
    pub fn new() -> Self {
        Cache {
            arr: vec![EMPTY; CACHE_SIZE as usize],
        }
    }

    pub fn update(&mut self, remaining_cards: CardSet, cards_to_keep: CardSet) -> bool {
        let idx = ((remaining_cards.cards + 13 * remaining_cards.doubles
                   + 23 * cards_to_keep.cards + 43 * cards_to_keep.doubles) % CACHE_SIZE) as usize;
        //let k1 = (1 << 52) % CACHE_SIZE;
        //let k2 = (k1 * k1) % CACHE_SIZE;
        //let k3 = (k1 * k2) % CACHE_SIZE;
        //let idx = ((remaining_cards.cards % CACHE_SIZE
                    //+ k1 * (remaining_cards.doubles % CACHE_SIZE)
                    //+ k2 * (cards_to_keep.cards % CACHE_SIZE)
                    //+ k3 * (cards_to_keep.doubles % CACHE_SIZE)) % CACHE_SIZE) as usize;
        if self.arr[idx] == (remaining_cards, cards_to_keep) {
            true
        } else {
            self.arr[idx] = (remaining_cards, cards_to_keep);
            false
        }
    }
}



