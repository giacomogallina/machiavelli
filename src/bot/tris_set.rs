#[derive(Copy, Clone, Debug)]
pub struct TrisSet {
    a: u128,
    b: u128,
}

impl TrisSet {
    pub const fn new() -> Self {
        TrisSet {
            a: 0,
            b: 0,
        }
    }

    pub fn merge(&self, ts: TrisSet) -> TrisSet {
        TrisSet {
            a: self.a | ts.a,
            b: self.b | ts.b,
        }
    }

    pub fn remove(&self, ts: TrisSet) -> TrisSet {
        TrisSet {
            a: self.a & !ts.a,
            b: self.b & !ts.b,
        }
    }

    pub fn intersect(&self, ts: TrisSet) -> TrisSet {
        TrisSet {
            a: self.a & ts.a,
            b: self.b & ts.b,
        }
    }
    
    pub const fn set(&self, i: usize) -> TrisSet {
        if i < 128 {
            TrisSet {
                a: self.a | 1 << i,
                b: self.b,
            }
        } else {
            TrisSet {
                a: self.a,
                b: self.b | 1 << (i - 128),
            }
        }
    }

    pub fn count_tris(&self) -> usize {
        (self.a.count_ones() + self.b.count_ones()) as usize
    }
    
}


impl Iterator for TrisSet {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.a != 0 {
            let tz = self.a.trailing_zeros();
            self.a ^= 1 << tz;
            Some(tz as usize)
        } else if self.b != 0 {
            let tz = self.b.trailing_zeros();
            self.b ^= 1 << tz;
            Some(128 + tz as usize)
        } else {
            None
        }
    }
}
