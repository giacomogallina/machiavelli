pub fn card_symbol(c: usize) -> char {
    let n = c % 13;
    match n {
        12 => 'K',
        11 => 'Q',
        10 => 'J',
        9 => 'X',
        0 => 'A',
        x => ('0' as u8 + x as u8 + 1) as char,
    }
}

fn print_card(c: usize) {
    let s = c / 13;
    print!("{}{} ",
        card_symbol(c),
        match s {
            0 => '♥',
            1 => '♦',
            2 => '♣',
            _ => '♠',
        }
    )
}


#[derive(Clone, Copy, Debug, PartialEq)]
pub struct CardSet {
    pub cards: u64,
    pub doubles: u64,
}

impl CardSet {
    pub const fn new() -> Self {
        CardSet {
            cards: 0,
            doubles: 0,
        }
    }

    pub const fn from_card(c: usize) -> Self {
        CardSet {
            cards: 1 << c as u64,
            doubles: 0,
        }
    }

    pub const fn merge(&self, b: CardSet) -> Self {
        CardSet {
            cards: self.cards | b.cards,
            doubles: self.doubles | b.doubles | (self.cards & b.cards),
        }
    }

    pub fn remove(&self, cs: CardSet) -> Self {
        let new_cards = self.cards & (!cs.cards);
        let new_doubles = self.doubles & (!cs.doubles);
        CardSet {
            cards: new_cards | new_doubles,
            doubles: new_cards & new_doubles,
        }
    }

    pub fn contains(&self, cs: CardSet) -> bool {
        ((cs.cards & self.cards) == cs.cards) && ((cs.doubles & self.doubles) == cs.doubles)
    }

    pub const fn contains_card(&self, c: usize) -> bool {
        self.cards & (1 << c) != 0
    }

    pub fn tabular_print(&self) {
        for s in 0..4 {
            print!(" {} | ", ['♥','♦','♣','♠'][s]);
            for n in 0..13 {
                let c = 13*s + n;
                print!("{}{} ",
                    if self.cards & (1 << c) != 0 { card_symbol(c) } else {' '},
                    if self.doubles & (1 << c) != 0 { card_symbol(c) } else {' '}
                );
            }
            println!("| {}", ['♥','♦','♣','♠'][s]);
        }
    }

    pub fn print(&self) {
        for i in 0..52 {
            if self.cards & (1 << i) != 0 {
                print_card(i);
            }
            if self.doubles & (1 << i) != 0 {
                print_card(i);
            }
        }
        //println!();
    }

    pub fn from_string(s: String) -> Option<CardSet> {
        let mut cs = CardSet::new();
        for card in s.split_ascii_whitespace() {
            if card.len() == 2 {
                let card: Vec<char> = card.chars().collect();
                let n = match card[0] {
                    'K' => 12,
                    'k' => 12,
                    'Q' => 11,
                    'q' => 11,
                    'J' => 10,
                    'j' => 10,
                    'X' => 9,
                    'x' => 9,
                    'A' => 0,
                    'a' => 0,
                    chr if '1' <= chr && chr <= '9' => {
                        chr as u8 - '1' as u8
                    },
                    _ => return None,
                };
                let s = match card[1] {
                    'c' => 0,
                    'q' => 1,
                    'f' => 2,
                    'p' => 3,
                    _ => return None,
                };
                cs = cs.merge(CardSet::from_card((n + 13 * s) as usize));
            } else {
                return None;
            }
        }
        Some(cs)
    }

    pub fn score(&self) -> u32 {
        self.cards.count_ones() + 3* self.doubles.count_ones()
    }
}

impl Iterator for CardSet {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cards != 0 {
            let tz = self.cards.trailing_zeros();
            self.cards ^= 1 << tz;
            Some(tz as usize)
        } else {
            None
        }
    }
}
