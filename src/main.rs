use std::io::{self, stdout, Write};
use std::time::Duration;
use crossterm::{
    execute,
    Result,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen, Clear, ClearType, enable_raw_mode, disable_raw_mode, size},
    cursor::{MoveTo, MoveDown, MoveUp, MoveLeft, MoveRight, SavePosition, RestorePosition, Hide, Show, MoveToNextLine, MoveToColumn},
    event::{poll, read, Event, KeyEvent, KeyCode, KeyModifiers},
};

mod bot;
use bot::{ best_move, BestMoveFeedback, card_set::{CardSet, card_symbol} };

fn read_line() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input
}

fn read_cards() -> CardSet {
    let mut cards = CardSet::from_string(read_line());
    while let None = cards {
        println!("hai scritto male qualcosa, riprova");
        cards = CardSet::from_string(read_line());
    }
    cards.unwrap()
}

fn flush() {
    execute!(stdout(), Clear(ClearType::All), MoveTo(0, 0)).unwrap();
}

fn tabular_print(cs: CardSet) {
    for s in 0..4 {
        execute!(stdout(), SavePosition).unwrap();
        print!(" {} | ", ['♥','♦','♣','♠'][s]);
        for n in 0..13 {
            let c = 13*s + n;
            print!("{}{} ",
                if cs.cards & (1 << c) != 0 { card_symbol(c) } else {' '},
                if cs.doubles & (1 << c) != 0 { card_symbol(c) } else {' '}
            );
        }
        print!("| {}", ['♥','♦','♣','♠'][s]);
        execute!(stdout(), RestorePosition, MoveDown(1)).unwrap();
    }
}

fn ui() {
    let mut hist: Vec<(CardSet, CardSet)> = vec![];
    loop {
        flush();
        if let Some((table, hand)) = hist.last() {
            println!("tavolo:");
            table.print();
            println!("mano:");
            hand.print();
            println!();
        }
        if hist.len() == 0 {
            println!("cosa c'e' in tavola?");
            let table = read_cards();
            println!("cos'hai in mano?");
            let hand = read_cards();
            hist.push((table, hand));
        } else if hist.len() % 2 == 1 {
            let (table, hand) = *hist.last().unwrap();
            println!("cos'hanno messo giu'?");
            let new_table = table.merge(read_cards());
            hist.push((new_table, hand));
        } else {
            let (table, hand) = *hist.last().unwrap();
            let feedback_rx = best_move(table, hand);
            let mut best_trises = vec![];
            let mut best_score = hand.score();
            for msg in feedback_rx.iter() {
                match msg {
                    BestMoveFeedback::Progress(x) => {
                        println!("{}%", x*100.0);
                    },
                    BestMoveFeedback::NewBestScore(score, trises) => {
                        best_score = score;
                        best_trises = trises;
                    },
                    _ => (),
                }
            }

            if best_score < hand.score() {
                println!("fai questi tris:");
                let mut new_table = CardSet::new();
                for t in best_trises {
                    t.print();
                    println!();
                    new_table = new_table.merge(t);
                }
                let new_hand = hand.remove(new_table.remove(table));
                hist.push((new_table, new_hand));
                read_line();
            } else {
                println!("pesca :(");
                println!("cos'hai pescato?");
                hist.push((table, hand.merge(read_cards())));
            }
        }
        while let Some((table, hand)) = hist.last() {
            println!("\ntavolo:");
            table.print();
            println!("mano:");
            hand.print();
            println!("\ntutto ok?");
            if read_line().contains("n") {
                hist.pop();
            } else {
                break;
            }
        }
    }
}

#[derive(Copy, Clone)]
enum Deck {
    Table = 0,
    Hand = 1,
}

enum ProgramMode {
    Normal,
    ChooseNumber(Deck),
    ChooseSeed(Deck, usize),
    Edit(Deck, usize, usize),
    Thinking,
    ShowResult,
    Quit,
}


fn ui2() -> Result<()> {
    enable_raw_mode()?;
    execute!(stdout(), EnterAlternateScreen, Hide)?;
    let mut mode = ProgramMode::Normal;
    let mut hist = vec![[CardSet::new(), CardSet::new()]];
    let mut hist_idx = 0;
    let mut best_trises = vec![];
    let mut best_score = 999;
    'main: loop {
        flush();
        for (i, s) in ["TABLE", "HAND"].iter().enumerate() {
            execute!(stdout(), MoveTo(26 + 55*i as u16, 1))?;
            print!("{}", s);
            execute!(stdout(), MoveTo(5 + 55*i as u16, 3))?;
            tabular_print(hist[hist_idx][i]);
        }
        execute!(stdout(), MoveTo(3, size()?.1 - 2))?;
        match mode {
            ProgramMode::Normal => {
                print!("NORMAL MODE"); stdout().flush()?;
                if let Event::Key(KeyEvent { code: c, modifiers: m }) = read()? {
                    match (c, m) {
                        (KeyCode::Char('h'), _) => {
                            mode = ProgramMode::ChooseNumber(Deck::Hand);
                        },
                        (KeyCode::Char('t'), _) => {
                            mode = ProgramMode::ChooseNumber(Deck::Table);
                        },
                        (KeyCode::Enter, _) => {
                            mode = ProgramMode::Thinking;
                        },
                        (KeyCode::Char('u'), _) => {
                            if hist_idx > 0 {
                                hist_idx -= 1;
                            }
                        }
                        (KeyCode::Char('r'), KeyModifiers::CONTROL) => {
                            if hist_idx < hist.len() - 1 {
                                hist_idx += 1;
                            }
                        },
                        (KeyCode::Char('c'), KeyModifiers::CONTROL) => {
                            mode = ProgramMode::Quit;
                        },
                        _ => (),
                    }
                }
            },
            ProgramMode::ChooseNumber(deck) => {
                print!("EDIT {} MODE", ["TABLE", "HAND"][deck as usize]);
                execute!(stdout(), MoveTo(23 + 55*deck as u16, 8))?;
                print!(">      ");
                stdout().flush()?;
                loop {
                    if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                        match c {
                            KeyCode::Esc => {
                                mode = ProgramMode::Normal; continue 'main;
                            },
                            KeyCode::Char('k') => { mode = ProgramMode::ChooseSeed(deck, 12); break; },
                            KeyCode::Char('q') => { mode = ProgramMode::ChooseSeed(deck, 11); break; },
                            KeyCode::Char('j') => { mode = ProgramMode::ChooseSeed(deck, 10); break; },
                            KeyCode::Char('x') => { mode = ProgramMode::ChooseSeed(deck, 9); break; },
                            KeyCode::Char('a') => { mode = ProgramMode::ChooseSeed(deck, 0); break; },
                            KeyCode::Char(chr) if '1' <= chr && chr <= '9' => {
                                mode = ProgramMode::ChooseSeed(deck, (chr as u8 - '1' as u8) as usize); break;
                            },
                            _ => (),
                        }
                    }
                }
            },
            ProgramMode::ChooseSeed(deck, n) => {
                print!("EDIT {} MODE", ["TABLE", "HAND"][deck as usize]);
                execute!(stdout(), MoveTo(23 + 55*deck as u16, 8))?;
                print!("> {}     ", card_symbol(n));
//['♥','♦','♣','♠'][s]
                stdout().flush()?;
                loop {
                    if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                        match c {
                            KeyCode::Esc => {
                                mode = ProgramMode::Normal; continue 'main;
                            },
                            KeyCode::Backspace => { mode = ProgramMode::ChooseNumber(deck); break; },
                            KeyCode::Char('c') => { mode = ProgramMode::Edit(deck, n, 0); break; },
                            KeyCode::Char('q') => { mode = ProgramMode::Edit(deck, n, 1); break; },
                            KeyCode::Char('f') => { mode = ProgramMode::Edit(deck, n, 2); break; },
                            KeyCode::Char('p') => { mode = ProgramMode::Edit(deck, n, 3); break; },
                            KeyCode::Char('0') if n == 0 => { mode = ProgramMode::ChooseSeed(deck, 9); break; },
                            _ => (),
                        }
                    }
                }
            },
            ProgramMode::Edit(deck, n, s) => {
                print!("EDIT {} MODE", ["TABLE", "HAND"][deck as usize]);
                execute!(stdout(), MoveTo(23 + 55*deck as u16, 8))?;
                print!("> {}{}     ", card_symbol(n), ['♥','♦','♣','♠'][s]);
                stdout().flush()?;
                loop {
                    if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                        match c {
                            KeyCode::Esc => {
                                mode = ProgramMode::Normal; break;
                            },
                            KeyCode::Backspace => { mode = ProgramMode::ChooseSeed(deck, n); break; },
                            KeyCode::Enter | KeyCode::Char(' ') => { 
                                hist.truncate(hist_idx + 1);
                                hist.push(hist[hist_idx]);
                                hist_idx += 1;
                                hist[hist_idx][deck as usize] = hist[hist_idx][deck as usize].merge(CardSet::from_card(13*s + n));
                                mode = ProgramMode::ChooseNumber(deck);
                                break;
                            },
                            KeyCode::Delete => {
                                hist.truncate(hist_idx + 1);
                                hist.push(hist[hist_idx]);
                                hist_idx += 1;
                                hist[hist_idx][deck as usize] = hist[hist_idx][deck as usize].remove(CardSet::from_card(13*s + n));
                                mode = ProgramMode::ChooseNumber(deck);
                                break;
                            },
                            _ => (),
                        }
                    }
                }
            },
            ProgramMode::Thinking => {
                let [table, hand] = hist[hist_idx];
                let feedback_rx = best_move(table, hand);
                best_score = hand.score();
                best_trises = vec![];
                let mut progress = 0.0;

                print!("THINKING MODE"); stdout().flush()?;
                loop {
                    for msg in feedback_rx.try_iter() {
                        match msg {
                            BestMoveFeedback::Progress(x) => {
                                progress = x*100.0;
                            },
                            BestMoveFeedback::NewBestScore(score, trises) => {
                                best_score = score;
                                best_trises = trises;
                            },
                            BestMoveFeedback::Done => {
                                mode = ProgramMode::ShowResult;
                                continue 'main;
                            },
                        }
                    }
                    execute!(stdout(), MoveTo(10, 10))?;
                    print!("Search is at {:.3}%, best score found so far is {}. Press ESC to stop     ", progress, best_score);
                    stdout().flush()?;
                    if poll(Duration::from_millis(100)).unwrap() {
                        if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                            match c {
                                KeyCode::Esc => {
                                    mode = ProgramMode::ShowResult;
                                    continue 'main;
                                },
                                _ => (),
                            }
                        }
                    }
                }
            },
            ProgramMode::ShowResult => {
                let [table, hand] = hist[hist_idx];
                execute!(stdout(), MoveTo(10, 10))?;
                let mut new_table = CardSet::new();
                let mut new_hand = CardSet::new();
                if best_score < hand.score() {
                    print!("Do these trises:");
                    for (i, t) in best_trises.iter().enumerate() {
                        execute!(stdout(), MoveTo(10 + 20*(i%3) as u16, 12 + (i/3) as u16))?;
                        t.print();
                        new_table = new_table.merge(*t);
                    }
                    new_hand = hand.remove(new_table.remove(table));
                } else {
                    print!("Draw :(");
                }
                execute!(stdout(), MoveToNextLine(2), MoveToColumn(10))?;
                print!("Press ENTER to play as suggested, or ESC to cancel"); stdout().flush()?;
                loop {
                    if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                        match c {
                            KeyCode::Esc => {
                                mode = ProgramMode::Normal;
                                continue 'main;
                            },
                            KeyCode::Enter => {
                                if best_score < hand.score() {
                                    mode = ProgramMode::Normal;
                                    hist.truncate(hist_idx+1);
                                    hist.push([new_table, new_hand]);
                                    hist_idx += 1;
                                } else {
                                    mode = ProgramMode::ChooseNumber(Deck::Hand);
                                }
                                continue 'main;
                            },
                            _ => (),
                        }
                    }
                }
            },
            ProgramMode::Quit => {
                print!("Do you want to quit? [y/n]"); stdout().flush()?;
                loop {
                    if let Event::Key(KeyEvent { code: c, .. }) = read()? {
                        match c {
                            KeyCode::Esc | KeyCode::Char('n') => {
                                mode = ProgramMode::Normal;
                                break;
                            },
                            KeyCode::Char('y') => {
                                break 'main;
                            }
                            _ => (),
                        }
                    }
                }
            },
        }
    }
    disable_raw_mode()?;
    execute!(stdout(), LeaveAlternateScreen, Show)?;
    Ok(())
}

fn main() {
    ui2().unwrap();
}
